# BornhackBadge_2022

This source is written for the https://github.com/bornhack/badge2022

## License
The source written by the author is released under the BSD 3-Clause license:
https://opensource.org/licenses/BSD-3-Clause

The included libraries are released under their respective licenses.

