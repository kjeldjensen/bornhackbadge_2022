print("Hello BornHack!")
# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

'''
2022-08-03 gpskjeld kj@kjen.dk Based on the original BornhackBadge code.py
                               Code restructure, added adafruit_bitmap_font
                               and gauge libraries
2022-08-07 gpskjeld kj@kjen.dk Changed how the slideshow lib is used
'''

import time
import os
import board
import microcontroller
import terminalio
import displayio
import digitalio
import pwmio
from adafruit_bitmap_font import bitmap_font
from adafruit_display_text import label
from adafruit_st7735r import ST7735R
from adafruit_slideshow import PlayBackOrder, SlideShow
from gauge import Gauge

font_file = "fonts/LeagueSpartan-Bold-16.bdf"
font = bitmap_font.load_font(font_file)

BACKLIGHT = False
BTN_A = False; BTN_B = False; BTN_X = False; BTN_Y = False

def buttons_init():
	global BTN_A, BTN_B, BTN_X, BTN_Y
	BTN_A = digitalio.DigitalInOut(board.BTN_A)
	BTN_A.switch_to_input(pull=digitalio.Pull.UP)
	BTN_B = digitalio.DigitalInOut(board.BTN_B)
	BTN_B.switch_to_input(pull=digitalio.Pull.UP)
	BTN_X = digitalio.DigitalInOut(board.BTN_X)
	BTN_X.switch_to_input(pull=digitalio.Pull.UP)
	BTN_Y = digitalio.DigitalInOut(board.BTN_Y)
	BTN_Y.switch_to_input(pull=digitalio.Pull.UP)

def display_init():
	global BACKLIGHT
	# Setup BACKLIGHT (0-65535)
	BACKLIGHT = pwmio.PWMOut(board.PWM0, frequency=5000, duty_cycle=0)
	BACKLIGHT.duty_cycle = 65535

	# Release any resources currently in use for the displays
	displayio.release_displays()

	# Configure the display interface
	spi = board.SPI()
	tft_cs = board.CS
	tft_dc = board.D1
	display_bus = displayio.FourWire(
		spi, command=tft_dc, chip_select=tft_cs, reset=board.D0
	)
	display = ST7735R(display_bus, width=128, height=160, rotation=0, bgr=True, colstart=2, rowstart=1)
	return display

def show_slideshow():
	disp = displayio.Group()
	display.show(disp)
	slideshow = SlideShow(display,
					  folder="/images",
					  loop=False,
					  order=PlayBackOrder.ALPHABETICAL,
					  dwell=2)
	while slideshow.update():
		pass
	
def show_temp():
	global font
	# https://github.com/benevpi/Circuit-Python-Gauge

	# Create a new group
	disp = displayio.Group()
	display.show(disp)

	# Show the labels
	disp_group = displayio.Group(scale=1, x=10, y=15)
	disp_area = label.Label(font, text="BornHack", color=0xFF8C00)
	disp_group.append(disp_area); disp.append(disp_group)

	disp_group = displayio.Group(scale=1, x=10, y=35)
	disp_area = label.Label(terminalio.FONT, text="RP2040 temperature", color=0xffffff)
	disp_group.append(disp_area); disp.append(disp_group)

	# Show the gauge
	colour_fade=[0x00FF00,0x00FF00,0x00FF00,0xFFFF00,0xFFFF00,0xFFFF00,0xFFFF00,0xFF0000,0xFF0000,0xFF0000]
	temp_gauge = Gauge(20,40, 80, 60, value_label="Temp: ", arc_colour=colour_fade, colour_fade=True)
	temp_gauge.x = 15
	temp_gauge.y = 40
	disp_group.append (temp_gauge)
	
	# Return the gauge object to enable updating
	return temp_gauge
	
# Main begins here
buttons_init()
display = display_init()
show_slideshow()
temp_gauge = show_temp()

# Keep updating the gauge until X is pressed
quit = False
while quit == False:
	time.sleep(0.2)
	temp_gauge.update(microcontroller.cpu.temperature)
	if BTN_X.value == False:
		quit = True

# Perform a harware reset
microcontroller.reset()

